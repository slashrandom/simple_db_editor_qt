/*
 *	dbeditor.cpp
 *  This file is part of "Simple DB Editor"
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "dbeditor.h"
#include "toolbox.h"
#include <QLabel>
#include <QPlainTextEdit>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlTableModel>
#include <QDebug>


DbEditor::DbEditor(QLabel *statusbar)
    : mStatusbar(statusbar)
{
    this->hide();
}

DbEditor::~DbEditor()
{
    if (mActiveDB && mActiveDB->isOpen())
        mActiveDB->close();
}

void DbEditor::Find(QString s)
{
    QSqlQuery query;
    for (int i=0; i<mSqlTable->columnCount(); ++i)
    {
        QString str = "SELECT "
                      + mSqlTable->headerData(i, Qt::Horizontal).toString()
                      + " FROM "
                      + db_table
                      + " WHERE "
                      + mSqlTable->headerData(i, Qt::Horizontal).toString()
                      + " = " + s;
        query.exec(str);
        while (query.next())
        {
            QString value = query.value(0).toString();
            qDebug() << value;
        }
    }

    // TODO: Update model with results....
}

void DbEditor::CloseDB()
{
    if (mActiveDB->isOpen())
        mActiveDB->close();

    if (mSqlTable)
    {
        delete mSqlTable;
        mSqlTable = nullptr;
    }

    if (mStatusbar)
        mStatusbar->clear();

    if (mActiveDB)
    {
        delete mActiveDB;
        mActiveDB = nullptr;
    }

    this->hide();
}

bool DbEditor::OpenDB(QPlainTextEdit *status,
                      QString driver,
                      QString server,
                      QString name,
                      QString table,
                      QString user,
                      QString passw,
                      int port)
{
    db_table = table;

    if (!mActiveDB)
    {
        mActiveDB = new QSqlDatabase(QSqlDatabase::addDatabase(driver));
        mActiveDB->setHostName(server);
        mActiveDB->setPort(port);
        mActiveDB->setDatabaseName(name);
        mActiveDB->setUserName(user);
        mActiveDB->setPassword(passw);
        mActiveDB->open();
    }

    if (!mActiveDB->isOpen())
    {
        qDebug() << mActiveDB->lastError();
        status->appendPlainText(mActiveDB->lastError().text());
        return false;
    }

    if (db_table.isEmpty())
    {
        QStringList list = mActiveDB->tables();
        QString str = list.join("");
        str = list.join(", ");
        status->appendPlainText("Available TABLES: " + str);
        return false;
    }
    else
    {
        mStatusbar->setText(user
                            + "@"
                            + server
                            + ":"
                            + name
                            + ":"
                            + table
                            + "  --  [F1] Toolbox -- [F4] SqlConsole"
                            );
        ReadDB();
    }

    return true;
}

void DbEditor::ReadDB()
{
    /* Read data from db */
    mSqlTable = new QSqlTableModel(nullptr, *mActiveDB);
    mSqlTable->setTable(db_table);

    /*
     * QSqlTableModel::OnFieldChange	0	All changes to the model will be
     *                                      applied immediately to the database.
     * QSqlTableModel::OnRowChange      1	Changes to a row will be applied
     *                                      when the user selects a different row.
     * QSqlTableModel::OnManualSubmit	2	All changes will be cached in the
     *                                      model until either submitAll() or
     *                                      revertAll() is called.
     */

    mSqlTable->setEditStrategy(QSqlTableModel::OnFieldChange);
    mSqlTable->select();
    this->setModel(mSqlTable);

    // Read and set column-headers
    for (int i=0; i<this->model()->columnCount(); i++)
        mSqlTable->setHeaderData(
                    i, Qt::Horizontal,
                    (this->model()->headerData(i, Qt::Horizontal).toString())
                    );

    this->resizeColumnsToContents();
    this->show();
}
