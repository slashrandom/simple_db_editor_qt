/*
 *	sqlconsole.cpp
 *  This file is part of "Simple DB Editor"
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sqlconsole.h"
#include "dbeditor.h"
#include <QAction>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QSqlError>
#include <QSqlQuery>
#include <QToolButton>
#include <QDebug>

SqlConsole::SqlConsole(DbEditor *dbe)
    : mDbEditor(dbe)
{
    /* Define a grid-layout */
    QGridLayout *grid = new QGridLayout;

    /* Add text-labels */
    QLabel *label_input  = new QLabel;
    QLabel *label_output = new QLabel;

    label_input->setText("Command line:");
    label_output->setText("Status:");

    /* Add input field */
    mCmdLine = new QLineEdit;

    /* Add output field */
    mStatusWin = new QPlainTextEdit;

    /* Create buttons */
    QToolButton *btn_exec = new QToolButton(this);
    QToolButton *btn_exit = new QToolButton(this);

    /* Create signals */
    QAction *act_exec = new QAction("", btn_exec);
    connect(act_exec, &QAction::triggered, this, &SqlConsole::Execute);

    QAction *act_hide = new QAction("", btn_exit);
    connect(act_hide, &QAction::triggered, this, &SqlConsole::HideWidget);

    /* Assign signals to buttons */
    btn_exec->setDefaultAction(act_exec);
    btn_exit->setDefaultAction(act_hide);

    /* Set fixed button-size */
    btn_exec->setFixedSize(110,32);
    btn_exit->setFixedSize(110,32);

    /* Setup grid-layout */
    grid->setSpacing(5);
    grid->setVerticalSpacing(3);
    grid->addWidget(label_input,0,0,1,1);
    grid->addWidget(mCmdLine,1,0,1,1);
    grid->addWidget(label_output,2,0,1,1);
    grid->addWidget(mStatusWin,3,0,1,3);
    grid->addWidget(btn_exec,1,1,1,1);
    grid->addWidget(btn_exit,1,2,1,1);
    this->setLayout(grid);

    /* Define widget size */
    this->setFixedHeight(350);

    /* Assign text to labels and buttons */
    btn_exec->setText("Exec");
    btn_exit->setText("Hide");

    this->hide();
}

void SqlConsole::ConsoleToggle()
{
    this->isVisible() ? this->hide() : this->show();

    if (this->isVisible())
        mCmdLine->setFocus();
}

bool SqlConsole::HasFocus()
{
    return mCmdLine->hasFocus();
}

void SqlConsole::Execute()
{
    if (!mDbEditor->GetActiveDB())
    {
        mStatusWin->appendPlainText("Database not connected!");
        return;
    }

    QString str(mCmdLine->text());
    if (str.isEmpty())
        return;

    QSqlQuery query(str, *mDbEditor->GetActiveDB());
    query.exec();

    mStatusWin->appendPlainText(">> " + str);

    while (query.next())
        mStatusWin->appendPlainText(query.value(0).toString());

    // Print error messages...
    if (query.lastError().isValid())
        mStatusWin->appendPlainText(query.lastError().text());
}

void SqlConsole::HideWidget()
{
    this->hide();
}
