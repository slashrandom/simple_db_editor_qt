/*
 *	toolbox.cpp
 *  This file is part of "Simple DB Editor"
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "toolbox.h"
#include "dbeditor.h"
#include "dbdialog.h"
#include <QAction>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QToolButton>
#include <QDebug>


Toolbox::Toolbox(QWidget *parent)
    : QWidget(parent)
{
    /* Define a grid-layout */
    QGridLayout *grid = new QGridLayout;

    /* Add text-labels */
    QLabel *label_search  = new QLabel;
    QLabel *label_replace = new QLabel;

    /* Add two enter-text fields */
    mSearchText  = new QLineEdit;
    mReplaceText = new QLineEdit;

    /* Create buttons */
    QToolButton *btn_insert_row = new QToolButton(this);
    QToolButton *btn_insert_col = new QToolButton(this);
    QToolButton *btn_delete_row = new QToolButton(this);
    QToolButton *btn_delete_col = new QToolButton(this);
    QToolButton *btn_find       = new QToolButton(this);
    QToolButton *btn_find_prev  = new QToolButton(this);
    QToolButton *btn_replace    = new QToolButton(this);
    QToolButton *btn_replaceall = new QToolButton(this);
    QToolButton *btn_hide       = new QToolButton(this);
    QToolButton *btn_closedb    = new QToolButton(this);

    /* Create signals */
    QAction *act_insert_row = new QAction("", btn_insert_row);
    connect(act_insert_row, &QAction::triggered, this, &Toolbox::InsertRow);

    QAction *act_insert_col = new QAction("", btn_insert_col);
    connect(act_insert_col, &QAction::triggered, this, &Toolbox::InsertCol);

    QAction *act_delete_row = new QAction("", btn_delete_row);
    connect(act_delete_row, &QAction::triggered, this, &Toolbox::DeleteRow);

    QAction *act_delete_col = new QAction("", btn_delete_col);
    connect(act_delete_col, &QAction::triggered, this, &Toolbox::DeleteCol);

    QAction *act_find = new QAction("", btn_find);
    connect(act_find, &QAction::triggered, this, &Toolbox::Find);

    QAction *act_find_prev = new QAction("", btn_find_prev);
    connect(act_find_prev, &QAction::triggered, this, &Toolbox::FindPrev);

    QAction *act_replace = new QAction("", btn_replace);
    connect(act_replace, &QAction::triggered, this, &Toolbox::Replace);

    QAction *act_replaceall = new QAction("", btn_replaceall);
    connect(act_replaceall, &QAction::triggered, this, &Toolbox::ReplaceAll);

    QAction *act_hide = new QAction("", btn_hide);
    connect(act_hide, &QAction::triggered, this, &Toolbox::HideWidget);

    QAction *act_closedb = new QAction("", btn_closedb);
    connect(act_closedb, &QAction::triggered, this, &Toolbox::CloseDb);

    /* Assign signals to buttons */
    btn_insert_row->setDefaultAction(act_insert_row);
    btn_insert_col->setDefaultAction(act_insert_col);
    btn_delete_row->setDefaultAction(act_delete_row);
    btn_delete_col->setDefaultAction(act_delete_col);
    btn_find->setDefaultAction(act_find);
    btn_find_prev->setDefaultAction(act_find_prev);
    btn_replace->setDefaultAction(act_replace);
    btn_replaceall->setDefaultAction(act_replaceall);
    btn_hide->setDefaultAction(act_hide);
    btn_closedb->setDefaultAction(act_closedb);

    /* Set fixed size labels */
    label_search->setFixedWidth(90);
    label_replace->setFixedWidth(90);

    /* Set fixed button-size */
    btn_insert_row->setFixedSize(110,32);
    btn_insert_col->setFixedSize(110,32);
    btn_delete_row->setFixedSize(110,32);
    btn_delete_col->setFixedSize(110,32);
    btn_find->setFixedSize(110,32);
    btn_find_prev->setFixedSize(110,32);
    btn_replace->setFixedSize(110,32);
    btn_replaceall->setFixedSize(110,32);
    btn_hide->setFixedSize(110,32);
    btn_closedb->setFixedSize(110,32);

    /* Define and setup grid-layout */
    grid->setSpacing(5);
    grid->setVerticalSpacing(3);
    grid->addWidget(label_search,0,0,1,1);
    grid->addWidget(mSearchText,0,1,1,1);
    grid->addWidget(label_replace,1,0,1,1);
    grid->addWidget(mReplaceText,1,1,1,1);
    grid->addWidget(btn_find,0,2,1,1);
    grid->addWidget(btn_find_prev,0,3,1,1);
    grid->addWidget(btn_replace,1,2,1,1);
    grid->addWidget(btn_replaceall,1,3,1,1);
    grid->addWidget(btn_insert_row,0,4,1,1);
    grid->addWidget(btn_insert_col,1,4,1,1);
    grid->addWidget(btn_delete_row,0,5,1,1);
    grid->addWidget(btn_delete_col,1,5,1,1);
    grid->addWidget(btn_hide,1,6,1,1);
    grid->addWidget(btn_closedb,0,6,1,1);
    this->setLayout(grid);

    /* Define widget size */
    this->setFixedHeight(90);
    this->setFixedWidth(800);

    /* Assign text to labels and buttons */
    btn_insert_row->setText("Ins Row");
    btn_insert_col->setText("Ins Col");
    btn_delete_row->setText("Del Row");
    btn_delete_col->setText("Del Col");
    btn_find->setText("Find");
    btn_find_prev->setText("Find Prev");
    btn_replace->setText("Replace");
    btn_replaceall->setText("Replace All");
    btn_hide->setText("Hide Toolbox");
    btn_closedb->setText("Close DB");
    label_search->setText("Find:");
    label_replace->setText("Replace:");

    /* Start hidden */
    this->hide();
}

void Toolbox::ToolboxToggle()
{
    this->isVisible() ? this->hide() : this->show();
}

void Toolbox::CloseDb()
{
    if (mDbEditor)
        mDbEditor->CloseDB();
    mDbDialog->show();
    this->hide();
}

void Toolbox::HideWidget()
{
    this->hide();
}

void Toolbox::DeleteCol()
{
    qDebug() << "Toolbox:DeleteCol";
}

void Toolbox::DeleteRow()
{
    qDebug() << "Toolbox:DeleteRow";
}

void Toolbox::Find()
{
    mDbEditor->Find(mSearchText->text());
}

void Toolbox::FindPrev()
{
    qDebug() << "Toolbox:FindPrev";
}

void Toolbox::InsertCol()
{
    qDebug() << "Toolbox:InsertCol";
}

void Toolbox::InsertRow()
{
    qDebug() << "Toolbox:InsertRow";
}

void Toolbox::Replace()
{
    qDebug() << "Toolbox:Replace";
}

void Toolbox::ReplaceAll()
{
    qDebug() << "Toolbox:ReplaceAll";
}

