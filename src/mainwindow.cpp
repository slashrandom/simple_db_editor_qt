/*
 *	mainwindow.cpp
 *  This file is part of "Simple DB Editor"
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "dbdialog.h"
#include "dbeditor.h"
#include "sqlconsole.h"
#include "toolbox.h"
#include <QDesktopWidget>
#include <QGridLayout>
#include <QKeyEvent>
#include <QLabel>
#include <QWidget>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{   
    mMainWindow = new QWidget();
    mStatusbar  = new QLabel;
    mToolbox    = new Toolbox(this);
    mDbEditor   = new DbEditor(mStatusbar);
    mDbDialog   = new DbDialog(this, mDbEditor);
    mConsole    = new SqlConsole(mDbEditor);
    mToolbox->mDbEditor = mDbEditor;
    mToolbox->mDbDialog = mDbDialog;

    // Set window and layout properties
    QGridLayout *grid = new QGridLayout;
    grid->addWidget(mDbDialog);
    grid->addWidget(mDbEditor);
    grid->addWidget(mToolbox);
    grid->addWidget(mConsole);
    grid->addWidget(mStatusbar);
    mMainWindow->setLayout(grid);
    setCentralWidget(mMainWindow);

    // Resize app to 80% of total screensize.
    resize(QDesktopWidget().availableGeometry(this).size() * 0.8);
}

MainWindow::~MainWindow()
{
    // TODO: Verify db sync before closing.
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_F1)
        mToolbox->ToolboxToggle();
    else if (e->key() == Qt::Key_F4)
        mConsole->ConsoleToggle();
    else if ((e->key() == Qt::Key_Return) && mConsole->HasFocus())
        mConsole->Execute();
}
