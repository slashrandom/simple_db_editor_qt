/*
 *	dbdialog.cpp
 *  This file is part of "Simple DB Editor"
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "dbdialog.h"
#include "dbeditor.h"
#include <QAction>
#include <QApplication>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QToolButton>
#include <QDebug>


DbDialog::DbDialog(QWidget *parent, DbEditor *dbe)
    : QWidget(parent)
    , mDbEditor(dbe)
{
    /* Define a grid-layout */
    QGridLayout *grid = new QGridLayout;

    /* Add text-labels */
    QLabel *label_title   = new QLabel;
    QLabel *label_driver  = new QLabel;
    QLabel *label_server  = new QLabel;
    QLabel *label_port    = new QLabel;
    QLabel *label_name    = new QLabel;
    QLabel *label_table   = new QLabel;
    QLabel *label_user    = new QLabel;
    QLabel *label_passw   = new QLabel;
    QLabel *label_info    = new QLabel;
    mStatusField          = new QPlainTextEdit;

    label_title->setText("Database Setup:");
    label_driver->setText("DB Driver:");
    label_server->setText("DB Server:");
    label_port->setText("DB Server Port:");
    label_name->setText("DB Name:");
    label_table->setText("DB Table:");
    label_user->setText("DB User:");
    label_passw->setText("DB Password:");
    label_info->setText(" \
    Driver Type \t Description \n \
    --------------------------------------------------------------------------------------- \n \
    QDB2 \t IBM DB2 \n \
    QIBASE \t Borland InterBase Driver \n \
    QMYSQL \t MySQL \\ Maria Driver \n \
    QOCI \t\t Oracle Call Interface Driver \n \
    QODBC \t ODBC Driver (includes Microsoft SQL Server) \n \
    QPSQL \t PostgreSQL Driver \n \
    QSQLITE \t SQLite version 3 or above \n \
    QSQLITE2 \t SQLite version 2 \n \
    QTDS \t\t Sybase Adaptive Server"
    );

    /* Add text fields */
    eDriver   = new QLineEdit;
    eServer   = new QLineEdit;
    ePort     = new QLineEdit;
    eName     = new QLineEdit;
    eTable    = new QLineEdit;
    eUser     = new QLineEdit;
    ePassword = new QLineEdit;

    /* Create buttons */
    QToolButton *btn_connect = new QToolButton(this);
    QToolButton *btn_exit    = new QToolButton(this);

    /* Create signals */
    QAction *act_connect = new QAction("", btn_connect);
    connect(act_connect, &QAction::triggered, this, &DbDialog::Connect);

    QAction *act_exit = new QAction("", btn_exit);
    connect(act_exit, &QAction::triggered, this, &DbDialog::Exit);

    /* Assign signals to buttons */
    btn_connect->setDefaultAction(act_connect);
    btn_exit->setDefaultAction(act_exit);

    /* Set fixed size labels and fields */
    label_title->setFixedWidth(120);
    eDriver->setFixedWidth(200);
    eServer->setFixedWidth(200);
    ePort->setFixedWidth(200);
    eName->setFixedWidth(200);
    eTable->setFixedWidth(200);
    eUser->setFixedWidth(200);
    ePassword->setFixedWidth(200);

    /* Set fixed button-size */
    btn_connect->setFixedSize(110,32);
    btn_exit->setFixedSize(110,32);

    /* Setup grid-layout */
    grid->setSpacing(5);
    grid->setVerticalSpacing(3);
    grid->addWidget(label_title,0,0,1,1);
    grid->addWidget(label_info,0,2,9,1);
    grid->addWidget(label_driver,1,0,1,1);
    grid->addWidget(eDriver,1,1,1,1);
    grid->addWidget(label_server,2,0,1,1);
    grid->addWidget(eServer,2,1,1,1);
    grid->addWidget(label_port,3,0,1,1);
    grid->addWidget(ePort,3,1,1,1);
    grid->addWidget(label_name,4,0,1,1);
    grid->addWidget(eName,4,1,1,1);
    grid->addWidget(label_table,5,0,1,1);
    grid->addWidget(eTable,5,1,1,1);
    grid->addWidget(label_user,6,0,1,1);
    grid->addWidget(eUser,6,1,1,1);
    grid->addWidget(label_passw,7,0,1,1);
    grid->addWidget(ePassword,7,1,1,1);
    grid->addWidget(btn_connect,8,0,1,1);
    grid->addWidget(btn_exit,8,1,1,1);
    grid->addWidget(mStatusField,9,0,1,4);

    this->setLayout(grid);

    /* Assign text to labels and buttons */
    btn_connect->setText("Connect");
    btn_exit->setText("Exit");
    eDriver->setText(mDriver);
    eServer->setText(mServer);
    ePort->setText(mPort);
    eName->setText(mName);
    eTable->setText(mTable);
    eUser->setText(mUser);
    ePassword->setText(mPassword);
}

void DbDialog::Connect()
{
    mStatusField->clear();

    /* Verify db-setup */
    mDriver   = eDriver->text();
    mServer   = eServer->text();
    mPort     = ePort->text();
    mName     = eName->text();
    mTable    = eTable->text();
    mUser     = eUser->text();
    mPassword = ePassword->text();

    if (
            mDriver.isEmpty()
            || mServer.isEmpty()
            || mPort.isEmpty()
            || mName.isEmpty()
            || mUser.isEmpty()
            || mPassword.isEmpty()
       )
    {
        mStatusField->appendPlainText("Database setup is incomplete.");
        return;
    }

    bool bIsOpen = mDbEditor->OpenDB(
                       mStatusField,
                       mDriver,
                       mServer,
                       mName,
                       mTable,
                       mUser,
                       mPassword,
                       mPort.toInt()
                       );
    if (bIsOpen)
        this->hide();
}

void DbDialog::Exit()
{
    QApplication::quit();
}
