/*
 *	toolbox.h
 *  This file is part of "Simple DB Editor"
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TOOLBOX_H
#define TOOLBOX_H

#include <QWidget>

class QLineEdit;
class DbEditor;
class DbDialog;

class Toolbox : public QWidget
{
    Q_OBJECT

public:
    explicit Toolbox(QWidget *parent = nullptr);
    ~Toolbox() = default;
    void ToolboxToggle();
    DbDialog *mDbDialog{ nullptr };
    DbEditor *mDbEditor{ nullptr };

private:
    QLineEdit *mSearchText { nullptr };
    QLineEdit *mReplaceText{ nullptr };

private slots:
    void HideWidget();
    /*
     * TODO: 1. Implement the following functions...
     *       2. Decide if delete/insert should be available
     *          Db row/col delete/insert are low-level methods that operates
     *          directly on the database and should prob not be called directly...
     */
    void CloseDb();
    void DeleteCol();
    void DeleteRow();
    void Find();
    void FindPrev();
    void InsertCol();
    void InsertRow();
    void Replace();
    void ReplaceAll();
};

#endif // TOOLBOX_H
