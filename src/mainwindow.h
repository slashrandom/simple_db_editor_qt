/*
 *	mainwindow.h
 *  This file is part of "Simple DB Editor"
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class DbDialog;
class DbEditor;
class Toolbox;
class QLabel;
class SqlConsole;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

protected:
    void keyPressEvent(QKeyEvent *e) override;

private:
    DbDialog *mDbDialog  { nullptr };
    DbEditor *mDbEditor  { nullptr };
    QWidget  *mMainWindow{ nullptr };
    QLabel   *mStatusbar { nullptr };
    Toolbox  *mToolbox   { nullptr };
    SqlConsole *mConsole { nullptr };
};

#endif // MAINWINDOW_H
