/*
 *	sqlconsole.h
 *  This file is part of "Simple DB Editor"
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SQLCONSOLE_H
#define SQLCONSOLE_H

#include <QWidget>

class DbEditor;
class QLineEdit;
class QPlainTextEdit;

class SqlConsole : public QWidget
{
    Q_OBJECT

public:
    explicit SqlConsole(DbEditor *dbe);
    void ConsoleToggle();
    bool HasFocus();

private:
    DbEditor       *mDbEditor { nullptr };
    QLineEdit      *mCmdLine  { nullptr };
    QPlainTextEdit *mStatusWin{ nullptr };

public slots:
    void Execute();

private slots:
    void HideWidget();
};

#endif // SQLCONSOLE_H
