/*
 *	dbdialog.h
 *  This file is part of "Simple DB Editor"
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DBDIALOG_H
#define DBDIALOG_H

#include <QWidget>

class DbEditor;
class QLineEdit;
class QPlainTextEdit;

class DbDialog : public QWidget
{
    Q_OBJECT
public:
    explicit DbDialog(QWidget *parent = nullptr, DbEditor *dbe = nullptr);
    ~DbDialog() = default;

private:
    /* -- Default Database Setup (optional) -- */
    QString mDriver   = "QMYSQL";              // Db driver
    QString mServer   = "localhost";           // Server address
    QString mPort     = "0";                   // Server port (0 for default)
    QString mName     = "mydb";                // Database name
    QString mTable    = "Post";                // Database table
    QString mUser     = "qt";                  // Database user
    QString mPassword = "qt";                  // Db user-password
    /* --------------------------------------- */

    DbEditor  *mDbEditor{ nullptr };
    QLineEdit *eDriver  { nullptr };
    QLineEdit *eServer  { nullptr };
    QLineEdit *ePort    { nullptr };
    QLineEdit *eName    { nullptr };
    QLineEdit *eTable   { nullptr };
    QLineEdit *eUser    { nullptr };
    QLineEdit *ePassword{ nullptr };
    QPlainTextEdit *mStatusField{ nullptr };

private slots:
    void Connect();
    void Exit();
};

#endif // DBDIALOG_H
